cmake_minimum_required(VERSION 3.16)

project(student_entry VERSION 1.0)

# Contains source files for creating library
add_subdirectory(src)
# Contains main driver main function source file
add_subdirectory(app)
# Advanced C++ concepts

This repo is for practicing some advanced C++ concepts.

### ``const`` functions and pointers

All the ``getters`` i.e functions that only fetch some data and dont modify anything are defined as ``const`` functions.<br/>
And all the arguments of ``setters`` and other functions which need no modification in function body are also made ``const``.

### ``namespace`` and ``using`` keyword

- ``namespace`` is used in all to scope of identifiers in aparticular file.
- ``using`` keyword is used for using ``std`` namespace in this project

### Constructor and Destructors

Concepts covered:
- Purpose
- Implementations
- Constructor overloading

### STL containers

- Basic classification:
    - sequence containers
    - container adapters
    - associative containers
    - unordered associative containers
- Some insights:
    - every container is best suited for a particular purpose and usage which could be:
        - fixed/dynamic sized data-structure
        - Some containers are optimized is terms of time and memory usage for sepcific operations like random access/insertion, pushing/popping at ends, traversing etc
    - most of the functions in basic classification in a particular group are similar.
    - container adaptors can be implemented using different underlying containers.

### Class access modifiers

- ``PUBLIC`` : members defined in this block are accessible anywhere in program where the class is defined.
- ``PRIVATE`` : members defined in this block are accessible to class` public members.
- ``PROTECTED`` : similar to ``PRIVATE`` keyword but members defined in this block are accessible to class` public members and other child/friend classes.<br/>
**Note:** class members are by default in ``PRIVATE`` mode. 












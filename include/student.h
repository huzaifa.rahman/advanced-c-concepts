#include <string>
#include <map>
#include <vector>
#include <list>

class student
{
private:
  struct student_record
  {
    std::string roll_no;
    int age;
    float cgpa;
  } record;
  std::map<std::string /*subject name*/, int /*marks*/> result;

public:
  student(const std::string &roll_no, const int &age, const float &cgpa);
  ~student();
  const std::string getRollNo() const;
  void setRollNo(const std::string &roll_no);
  int getAge() const;
  void setAge(const int &age);
  const float getCGPA() const;
  void setCGPA(const float &cgpa);
};

class whitelist
{
private:
  std::map<std::string /* student name */, student * /* student entry pointer */> student_record;
  // vector<student> student_data_list;
  std::list<student /* student entry */> student_data_list;

public:
  whitelist();
  void add_to_whitelist(const std::string &student_name, const student &_student);
  const bool is_student_present(const std::string &student_name) const;
  const student *get_student_data(const std::string &student_name) const;
};

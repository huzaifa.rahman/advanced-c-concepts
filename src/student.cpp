#include "student.h"

using namespace std;

student::student(const string &roll_no, const int &age, const float &cgpa)
{
  student::setRollNo(roll_no);
  student::setAge(age);
  student::setCGPA(cgpa);
}
student::~student() {}
const string student::getRollNo() const
{
  return record.roll_no;
}
void student::setRollNo(const string &roll_no)
{
  record.roll_no = roll_no;
}
int student::getAge() const
{
  return record.age;
}
void student::setAge(const int &age)
{
  record.age = age;
}
const float student::getCGPA() const
{
  return record.cgpa;
}
void student::setCGPA(const float &cgpa)
{
  record.cgpa = cgpa;
}

void whitelist::add_to_whitelist(const string &student_name, const student &_student)
{
  student_data_list.push_back(_student);
  // student *st_ptr = new student(student_data_list.back());
  student_record.insert({student_name, &student_data_list.back()});
  // st_ptr = NULL;
  // delete st_ptr;
}
const bool whitelist::is_student_present(const string &student_name) const
{
  auto search = student_record.find(student_name);
  if (search != student_record.end())
  {
    return true;
  }
  else
  {
    return false;
  }
}
const student *whitelist::get_student_data(const string &student_name) const
{
  auto search = student_record.find(student_name);
  if (search != student_record.end())
  {
    return search->second;
  }
  else
  {
    return nullptr;
  }
}
whitelist::whitelist()
{
  // student_data_list.reserve(30);
}
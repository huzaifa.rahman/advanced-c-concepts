#include "student.h"

using namespace std;

//  To initialize student data in whitelist
inline void initailize(whitelist *_whitelist);

int main()
{
    whitelist _whitelist;
    // raw pointer to access student data
    const student *student_ptr;

    //  initialize data
    initailize(&_whitelist);

    //  student names
    string name[16] = {"huzaifa", "umair", "mueeb", "usman", "usama",
                       "jawad", "munib", "mozzam", "farakh", "fidaullah",
                       "ahmad", "yumna", "saad", "adnan", "talal",
                       //   for error checking
                       "unkown-name"};

    for (int i = 0; i < 16; i++)
    {
        //  get pointer of ith student entry
        student_ptr = _whitelist.get_student_data(name[i]);
        //  if its not unkown name then enter this block
        if (student_ptr != nullptr)
            // just formating
            printf("%-5s : %-15s %-5s : %-10s %-5s : %-5d %-5s : %-5.2f\n",
                   "name", name[i].c_str(),
                   "Roll#", student_ptr->getRollNo().c_str(),
                   "Age", student_ptr->getAge(),
                   "CGPA", student_ptr->getCGPA());
        else
            printf("Function : %s === ERROR!: \"%s\" not found\n", __FUNCTION__, name[i].c_str());
    }
    return 0;
}

inline void initailize(whitelist *_whitelist)
{
    student student1("1", 20 + 1, 1.5 + .1),
        student2("2", 20 + 2, 1.5 + .2),
        student3("3", 20 + 3, 1.5 + .3),
        student4("4", 20 + 4, 1.5 + .4),
        student5("5", 20 + 5, 1.5 + .5),
        student6("6", 20 + 6, 1.5 + .6),
        student7("7", 20 + 7, 1.5 + .7),
        student8("8", 20 + 8, 1.5 + .8),
        student9("9", 20 + 9, 1.5 + .9),
        student10("10", 20 + 10, 1.5 + 1),
        student11("11", 20 + 11, 1.5 + 1.1),
        student12("12", 20 + 12, 1.5 + 1.2),
        student13("13", 20 + 13, 1.5 + 1.3),
        student14("14", 20 + 14, 1.5 + 1.4),
        student15("15", 20 + 15, 1.5 + 1.5);

    _whitelist->add_to_whitelist("huzaifa", student1);
    _whitelist->add_to_whitelist("umair", student2);
    _whitelist->add_to_whitelist("mueeb", student3);
    _whitelist->add_to_whitelist("usman", student4);
    _whitelist->add_to_whitelist("usama", student5);
    _whitelist->add_to_whitelist("jawad", student6);
    _whitelist->add_to_whitelist("munib", student7);
    _whitelist->add_to_whitelist("mozzam", student8);
    _whitelist->add_to_whitelist("farakh", student9);
    _whitelist->add_to_whitelist("fidaullah", student10);
    _whitelist->add_to_whitelist("ahmad", student11);
    _whitelist->add_to_whitelist("yumna", student12);
    _whitelist->add_to_whitelist("saad", student13);
    _whitelist->add_to_whitelist("adnan", student14);
    _whitelist->add_to_whitelist("talal", student15);
}